package com.hustleapps.wordmix;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class activity_main extends ActionBarActivity {
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    String username;
    String randomQues="http://api.wordnik.com:80/v4/words.json/randomWord?hasDictionaryDef=true&minCorpusCount=0&maxCorpusCount=-1&minDictionaryCount=100&maxDictionaryCount=-1&minLength=5&maxLength=-1&api_key=a2a73e7b926c924fad7001ca3111acd55af2ffabf50eb4ae5";
    String defURLpre="http://api.wordnik.com:80/v4/word.json/";
    String defURLpost="/definitions?limit=100&includeRelated=true&sourceDictionaries=all&useCanonical=false&includeTags=true&api_key=a2a73e7b926c924fad7001ca3111acd55af2ffabf50eb4ae5";
    String relURLpre="http://api.wordnik.com:80/v4/word.json/";
    String relURLpost="relatedWords?useCanonical=false&limitPerRelationshipType=10&api_key=a2a73e7b926c924fad7001ca3111acd55af2ffabf50eb4ae5";
    private ProgressDialog pDialog;
    private String next_id;
    private String next_word;
    private ArrayList<String> next_def=new ArrayList<String>();
    private ArrayList<String> next_synon=new ArrayList<String>();
    private ArrayList<String> next_anton=new ArrayList<String>();
    private AlertDialog.Builder inputNameDailog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final EditText input=new EditText(this);
        final TextView scoreView=(TextView)findViewById(R.id.score);
        final TextView userView=(TextView)findViewById(R.id.username);
        final TextView startButton = (TextView)findViewById(R.id.startButton);

        sharedPreferences = getSharedPreferences("wordmix", Context.MODE_PRIVATE);
        boolean firstRun=sharedPreferences.getBoolean("firstrun", true);
        if(firstRun == true){
            input.setInputType(InputType.TYPE_CLASS_TEXT);
            inputNameDailog=new AlertDialog.Builder(activity_main.this);
            inputNameDailog.setTitle("Username");
            inputNameDailog.setView(input);
            inputNameDailog.setCancelable(false);

            inputNameDailog.setPositiveButton("done", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    editor = sharedPreferences.edit();
                    editor.putBoolean("firstrun", false);
                    editor.putString("wmusername", input.getText().toString());
                    editor.putString("wmscore", "20");
                    editor.commit();
                    username = input.getText().toString();
                    userView.setText(username);
                    scoreView.setText("20");
                }
            });
            inputNameDailog.show();
        }else{
            username = sharedPreferences.getString("wmusername","name");
            userView.setText(username);
            scoreView.setText(String.valueOf(sharedPreferences.getString("wmscore", "0")));
        }
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startgame = new Intent(activity_main.this, activity_game.class);
                startgame.putExtra("word", next_word);
                startgame.putStringArrayListExtra("def", next_def);
                startgame.putStringArrayListExtra("syn", next_synon);
                startgame.putStringArrayListExtra("ant", next_anton);
                startgame.putExtra("bestScore", String.valueOf(sharedPreferences.getString("wmscore", "20")));
                startgame.putExtra("currentScore", "20");
                startgame.putExtra("celebrate", "n");
                startActivity(startgame);
            }
        });
        new GetQuesFirst().execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    private class GetQuesFirst extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(activity_main.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();

        }
        @Override
        protected Void doInBackground(Void... params) {
            next_def=new ArrayList<String>();
            next_synon=new ArrayList<String>();
            next_anton=new ArrayList<String>();
            JSONObject jsonObj = null;
            ServiceHandler sh = new ServiceHandler();
            String jsonStrQues = sh.makeServiceCall(getApplicationContext(),randomQues,ServiceHandler.GET);
            if(jsonStrQues!=null){
                try{
                    jsonObj= new JSONObject(jsonStrQues);
                    next_id = jsonObj.getString("id");
                    next_word = jsonObj.getString("word");
                    if(next_word!=null){
                        String jsonStrDef = sh.makeServiceCall(getApplicationContext(),defURLpre + next_word + defURLpost,ServiceHandler.GET);
                        if(jsonStrQues!=null){
                            JSONArray jsonArrdef=new JSONArray(jsonStrDef);
                            for(int index=0;index<jsonArrdef.length();index++){
                                JSONObject jsonObjDef=jsonArrdef.getJSONObject(index);
                                next_def.add(android.text.Html.fromHtml(jsonObjDef.getString("text")).toString());
                            }
                        }
                        String jsonStrRel=sh.makeServiceCall(getApplicationContext(),relURLpre + next_word + relURLpost,ServiceHandler.GET);
                        if(jsonStrRel!=null){
                            JSONArray jsonArrRel=new JSONArray(jsonStrRel);
                            Log.d("json related",jsonArrRel.length()+"help");
                            for(int index=0;index<jsonArrRel.length();index++){
                                JSONObject jsonObjRel=jsonArrRel.getJSONObject(index);
                                for(int i2=0;i2<jsonObjRel.length();i2++){
                                    if(jsonObjRel.getString("relationshipType")=="antonym"){
                                        JSONArray jsonAnt=jsonObjRel.getJSONArray("words");
                                        for(int i3=0;i3<jsonAnt.length();i3++){
                                            next_anton.add(jsonAnt.getString(i3));
                                        }
                                    }
                                    if(jsonObjRel.getString("relationshipType")=="synonym"){
                                        JSONArray jsonAnt=jsonObjRel.getJSONArray("words");
                                        for(int i3=0;i3<jsonAnt.length();i3++){
                                            next_synon.add(jsonAnt.getString(i3));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void result){
            super.onPostExecute(result);
            if (pDialog.isShowing())
                pDialog.dismiss();
        }
    }
}

