package com.hustleapps.wordmix;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class activity_game extends ActionBarActivity {
    String randomQues="http://api.wordnik.com:80/v4/words.json/randomWord?hasDictionaryDef=true&minCorpusCount=0&maxCorpusCount=-1&minDictionaryCount=100&maxDictionaryCount=-1&minLength=5&maxLength=-1&api_key=a2a73e7b926c924fad7001ca3111acd55af2ffabf50eb4ae5";
    String defURLpre="http://api.wordnik.com:80/v4/word.json/";
    String defURLpost="/definitions?limit=100&includeRelated=true&sourceDictionaries=all&useCanonical=false&includeTags=true&api_key=a2a73e7b926c924fad7001ca3111acd55af2ffabf50eb4ae5";
    String relURLpre="http://api.wordnik.com:80/v4/word.json/";
    String relURLpost="relatedWords?useCanonical=false&limitPerRelationshipType=10&api_key=a2a73e7b926c924fad7001ca3111acd55af2ffabf50eb4ae5";
    int hint=1;
    private ProgressDialog pDialog;
    private String curr_id;
    private String curr_word;
    private List<String> curr_def;
    private List<String> curr_synon;
    private List<String> curr_anton;
    private String next_id;
    private String next_word;
    private ArrayList<String> next_def=new ArrayList<String>();
    private ArrayList<String> next_synon=new ArrayList<String>();
    private ArrayList<String> next_anton=new ArrayList<String>();
    private int currScore;
    private int bestScore;
    private String celebrate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        final TextView scoreView=(TextView)findViewById(R.id.scoreValue);
        TextView bestView=(TextView)findViewById(R.id.bestValue);
        TextView quesView=(TextView)findViewById(R.id.ques);
        final TextView hintBut=(TextView)findViewById(R.id.hintBut);
        final TextView hintView=(TextView)findViewById(R.id.hint);
        final TextView ansView=(TextView)findViewById(R.id.ans);
        TextView nextView=(TextView)findViewById(R.id.next);

        Bundle getExtra=getIntent().getExtras();
        if(getExtra!=null){
            curr_word=getExtra.getString("word");
            Log.w("word",curr_word);
            curr_def=getExtra.getStringArrayList("def");
            curr_synon=getExtra.getStringArrayList("syn");
            curr_anton=getExtra.getStringArrayList("ant");
            bestScore=Integer.parseInt(getExtra.getString("bestScore"));
            currScore=Integer.parseInt(getExtra.getString("currentScore"));
            celebrate=getExtra.getString("celebrate");
        }
        Log.e("celebrate",celebrate);
        if(celebrate!=null && celebrate.equals("y")){
            new AlertDialog.Builder(this)
                    .setTitle("Wooohoooo!")
                    .setMessage("Best Score!")
                    .setPositiveButton("Continue!", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    })
                    .show();

        }
        scoreView.setText(String.valueOf(currScore));
        //bestView.setText();
        final SharedPreferences sharedPreferences = getSharedPreferences("wordmix", Context.MODE_PRIVATE);
        bestView.setText(String.valueOf(sharedPreferences.getString("wmscore", "00")));
        String ques=null;
        Random r=new Random();
        int defIndex=r.nextInt(curr_def.size());
        ques = "def: "+curr_def.get(defIndex)+"\n";
        int synIndex=0;
        if(curr_synon.size()>0)
        {
            synIndex=r.nextInt(curr_synon.size());
            ques.concat("syn: "+curr_synon.get(synIndex)+"\n");
        }
        final int synIndex2=synIndex;

        int antIndex=0;
        if(curr_anton.size()>0)
        {
            antIndex=r.nextInt(curr_anton.size());
            ques.concat("ant: " + curr_anton.get(antIndex));
        }

        quesView.setText(ques);
        hintBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(curr_def.size()>hint) {
                    hintView.setText(hintView.getText().toString().concat("\n"+hint+".) "+curr_def.get(hint)));
                    hint++;
                }else
                    hintBut.setVisibility(View.GONE);
                    hintView.setText(hintView.getText().toString().concat("\n"+"thats it! no more hints"));
                hintView.setVisibility(View.VISIBLE);
            }
        });
        nextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ans = ansView.getText().toString();
                Log.d("answer", ans);
                if (ans.toLowerCase().equals(curr_word) || (curr_synon.contains(ans) && !ans.equals(curr_synon.get(synIndex2)))) {
                    Intent next = new Intent(activity_game.this, activity_game.class);
                    next.putExtra("callingActivity", "game");
                    currScore=Integer.parseInt(scoreView.getText().toString())+10;
                    if(currScore>bestScore){
                        SharedPreferences.Editor editor;
                        editor = sharedPreferences.edit();
                        editor.putString("wmscore", String.valueOf(currScore));
                        editor.commit();
                        if(celebrate.equals("n"))
                            celebrate="y";
                        else if(celebrate.equals("y"))
                            celebrate="d";
                        bestScore=currScore;
                    }
                    next.putExtra("currentScore", String.valueOf(currScore));
                    next.putExtra("bestScore", String.valueOf(bestScore));
                    next.putExtra("word", next_word);
                    next.putStringArrayListExtra("def", next_def);
                    next.putStringArrayListExtra("syn", next_synon);
                    next.putStringArrayListExtra("ant", next_def);
                    next.putExtra("celebrate",celebrate);
                    next.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(next);
                }else
                {   if(currScore>0)
                    {
                        currScore=Integer.parseInt(scoreView.getText().toString());
                        currScore=(currScore-2)-(hint-1);
                        scoreView.setText(String.valueOf(currScore));


                    }else{
                    new AlertDialog.Builder(activity_game.this)
                            .setTitle("Game over !!!")
                            .setMessage("Score : "+currScore)
                            .setPositiveButton("thanks!", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            });
                    }

                    ansView.setText("");
                    Toast.makeText(getApplicationContext(), "Try again!", Toast.LENGTH_SHORT).show();
                }
        }
        });
        new GetQuesFirst().execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_game, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            new AlertDialog.Builder(this)
                    .setTitle("Seriously?")
                    .setMessage("Word : "+curr_word)
                    .setPositiveButton("go to next", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent next = new Intent(activity_game.this, activity_game.class);
                            next.putExtra("callingActivity", "game");
                            next.putExtra("currentScore", String.valueOf(currScore));
                            next.putExtra("bestScore", String.valueOf(bestScore));
                            next.putExtra("word", next_word);
                            next.putStringArrayListExtra("def", next_def);
                            next.putStringArrayListExtra("syn", next_synon);
                            next.putStringArrayListExtra("ant", next_def);
                            next.putExtra("celebrate", celebrate);
                            next.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(next);
                            dialog.cancel();
                        }
                    })
                    .setNegativeButton("quit", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }})
                    .show();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onPause(){
        super.onPause();
    }
    private class GetQuesFirst extends AsyncTask<Void, Void, Void>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
        }
        @Override
        protected Void doInBackground(Void... params) {
            next_def=new ArrayList<String>();
            next_synon=new ArrayList<String>();
            next_anton=new ArrayList<String>();
            JSONObject jsonObj = null;
            ServiceHandler sh = new ServiceHandler();
            String jsonStrQues = sh.makeServiceCall(getApplicationContext(),randomQues,ServiceHandler.GET);
            if(jsonStrQues!=null){
                try{
                    jsonObj= new JSONObject(jsonStrQues);
                    next_id = jsonObj.getString("id");
                    next_word = jsonObj.getString("word");
                    if(next_word!=null){
                        String jsonStrDef = sh.makeServiceCall(getApplicationContext(),defURLpre + next_word + defURLpost,ServiceHandler.GET);
                        if(jsonStrQues!=null){
                            JSONArray jsonArrdef=new JSONArray(jsonStrDef);
                            for(int index=0;index<jsonArrdef.length();index++){
                                JSONObject jsonObjDef=jsonArrdef.getJSONObject(index);
                                next_def.add(jsonObjDef.getString("text"));
                            }
                        }
                        String jsonStrRel=sh.makeServiceCall(getApplicationContext(),relURLpre + next_word + relURLpost,ServiceHandler.GET);
                        if(jsonStrRel!=null){
                            JSONArray jsonArrRel=new JSONArray(jsonStrRel);
                            for(int index=0;index<jsonArrRel.length();index++){
                                JSONObject jsonObjRel=jsonArrRel.getJSONObject(index);
                                for(int i2=0;i2<jsonObjRel.length();i2++){
                                    if(jsonObjRel.getString("relationshipType")=="antonym"){
                                        JSONArray jsonAnt=jsonObjRel.getJSONArray("words");
                                        for(int i3=0;i3<jsonAnt.length();i3++){
                                            next_anton.add(jsonAnt.getString(index));
                                        }
                                    }
                                    if(jsonObjRel.getString("relationshipType")=="synonym"){
                                        JSONArray jsonAnt=jsonObjRel.getJSONArray("words");
                                        for(int i3=0;i3<jsonAnt.length();i3++){
                                            next_synon.add(jsonAnt.getString(index));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void result){
            super.onPostExecute(result);
        }
    }
}
